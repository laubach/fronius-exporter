module bitbucket.org/laubach/fronius-exporter

go 1.16

require (
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/knadh/koanf v1.1.1
	github.com/pelletier/go-toml v1.9.3 // indirect
	github.com/prometheus/client_golang v1.11.0
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/pflag v1.0.5
	github.com/stretchr/testify v1.7.0
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
