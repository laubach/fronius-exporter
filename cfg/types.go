package cfg

import "time"

type (
	// Configuration holds a strongly-typed tree of the configuration
	Configuration struct {
		Log      LogConfig  `koanf:"log"`
		Symo     SymoConfig `koanf:"symo"`
		BindAddr string     `koanf:"bind-addr"`
	}
	// LogConfig configures the logging options
	LogConfig struct {
		Level   string `koanf:"level"`
		Verbose bool   `koanf:"verbose"`
	}
	// SymoConfig configures the Fronius Symo device
	SymoConfig struct {
		URL           string        `koanf:"url"`
		MeterURL      string        `koanf:"meterUrl"`
		Timeout       time.Duration `koanf:"timeout"`
		Headers       []string      `koanf:"header"`
		CacheURL      string        `koanf:"cacheUrl"`
		BatteryURL    string        `koanf:"batteryURL"`
		PowerMeterURL string        `koanf:"powerMeterURL"`
	}
)

// NewDefaultConfig retrieves the hardcoded configs with sane defaults
func NewDefaultConfig() *Configuration {
	return &Configuration{
		Log: LogConfig{
			Level: "info",
		},
		Symo: SymoConfig{
			URL:           "http://gen24/solar_api/v1/GetPowerFlowRealtimeData.fcgi",
			MeterURL:      "http://gen24/solar_api/v1/GetMeterRealtimeData.cgi?Scope=Device&DeviceId=0",
			CacheURL:      "http://gen24/components/393216/readable",
			BatteryURL:    "http://gen24/components/16580608/readable",
			PowerMeterURL: "http://gen24/components/16711680/readable",
			Timeout:       5 * time.Second,
			Headers:       []string{},
		},
		BindAddr: ":8080",
	}
}
