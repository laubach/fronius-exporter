package main

import (
	"bitbucket.org/laubach/fronius-exporter/pkg/fronius"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	log "github.com/sirupsen/logrus"
	"time"
)

var (
	namespace           = "fronius"
	meterNamespace      = "froniusMeter"
	cacheNamespace      = "cache"
	gen24Namespace      = "gen24"
	batteryNamespace    = "battery"
	scrapeDurationGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "scrape_duration_seconds",
		Help:      "Time it took to scrape the device in seconds",
	})
	scrapeErrorCount = promauto.NewCounter(prometheus.CounterOpts{
		Namespace: namespace,
		Name:      "scrape_error_count",
		Help:      "Number of scrape errors",
	})

	inverterSOCGauge = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "battery_soc",
		Help:      "state of charge of the BYD battery",
	}, []string{"battery"})

	inverterPowerGaugeVec = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "inverter_power",
		Help:      "Power flow of the inverter in Watt",
	}, []string{"inverter"})

	sitePowerLoadGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "site_power_load",
		Help:      "Site power load in Watt",
	})
	sitePowerGridGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "site_power_grid",
		Help:      "Site power supplied to or provided from the grid in Watt",
	})
	sitePowerGridPurchaseGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "site_power_grid_purchase",
		Help:      "Site power provided from the grid in Watt",
	})
	sitePowerGridInjectGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "site_power_grid_inject",
		Help:      "Site power supplied to the grid in Watt",
	})
	sitePowerAccuGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "site_power_accu",
		Help:      "Site power supplied to or provided from the accumulator(s) in Watt",
	})
	sitePowerPhotovoltaicsGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "site_power_photovoltaic",
		Help:      "Site power supplied to or provided from the accumulator(s) in Watt",
	})

	siteAutonomyRatioGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "site_autonomy_ratio",
		Help:      "Relative autonomy ratio of the site",
	})
	siteSelfConsumptionRatioGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "site_selfconsumption_ratio",
		Help:      "Relative self consumption ratio of the site",
	})

	siteEnergyGaugeVec = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "site_energy_consumption",
		Help:      "Energy consumption in kWh",
	}, []string{"time_frame"})

	acbridgeEnergyactiveActiveconsumedSum01Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: cacheNamespace,
		Name:      "acbridge_energyactive_activeconsumed_sum01",
		Help:      "AcbridgeEnergyactiveActiveconsumedSum01",
	})

	acbridgeEnergyactiveActiveconsumedSum02Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: cacheNamespace,
		Name:      "acbridge_energyactive_activeconsumed_sum02",
		Help:      "AcbridgeEnergyactiveActiveconsumedSum02",
	})

	acbridgeEnergyactiveActiveconsumedSum03Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: cacheNamespace,
		Name:      "acbridge_energyactive_activeconsumed_sum03",
		Help:      "AcbridgeEnergyactiveActiveconsumedSum03",
	})

	acbridgeEnergyactiveProducedSum01Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: cacheNamespace,
		Name:      "acbridge_energyactive_produced_sum01",
		Help:      "AcbridgeEnergyactiveProducedSum01",
	})

	acbridgeEnergyactiveProducedSum02Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: cacheNamespace,
		Name:      "acbridge_energyactive_produced_sum02",
		Help:      "AcbridgeEnergyactiveProducedSum02",
	})

	acbridgeEnergyactiveProducedSum03Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: cacheNamespace,
		Name:      "acbridge_energyactive_produced_sum03",
		Help:      "AcbridgeEnergyactiveProducedSum03",
	})

	acbridgePoweractiveSumMeanF32Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: cacheNamespace,
		Name:      "acbridge_poweractive_sum_mean_f32",
		Help:      "AcbridgePoweractiveSumMeanF32",
	})

	acbridgePowerapparentSumMeanF32Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: cacheNamespace,
		Name:      "acbridge_powerapparent_sum_mean_f32",
		Help:      "AcbridgePowerapparentSumMeanF32",
	})

	acbridgePowerreactiveSumMeanF32Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: cacheNamespace,
		Name:      "acbridge_powerreactive_sum_mean_f32",
		Help:      "AcbridgePowerreactiveSumMeanF32",
	})

	acbridgeTimeBackupmodeUptimeSumF32Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: cacheNamespace,
		Name:      "acbridge_time_backupmode_uptime_sum_f32",
		Help:      "AcbridgeTimeBackupmodeUptimeSumF32",
	})

	batEnergyactiveActivechargeSum01Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: cacheNamespace,
		Name:      "bat_energyactive_activecharge_sum01",
		Help:      "BatEnergyactiveActivechargeSum01",
	})

	batEnergyactiveActivedischargeSum01Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: cacheNamespace,
		Name:      "bat_energyactive_activedischarge_sum01",
		Help:      "BatEnergyactiveActivedischargeSum01",
	})

	deviceTimeUptimeSumF32Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: cacheNamespace,
		Name:      "device_time_uptime_sum_f32",
		Help:      "DeviceTimeUptimeSumF32",
	})

	pvEnergyactiveActiveSum01Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: cacheNamespace,
		Name:      "pv_energyactive_active_sum01",
		Help:      "PvEnergyactiveActiveSum01",
	})
	pvEnergyactiveActiveSum02Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: cacheNamespace,
		Name:      "pv_energyactive_active_sum02",
		Help:      "PvEnergyactiveActiveSum02",
	})
	pvEnergyactiveActiveSumGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: cacheNamespace,
		Name:      "pv_energyactive_active_sum",
		Help:      "PvEnergyactiveActiveSum",
	})
	deviceTemperatureAmbientGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: cacheNamespace,
		Name:      "device_temperature_ambient",
		Help:      "device temperature ambient",
	})
	moduleTemperature01Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: cacheNamespace,
		Name:      "module_Temperature_1",
		Help:      "module temperature 1",
	})
	moduleTemperature03Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: cacheNamespace,
		Name:      "module_Temperature_3",
		Help:      "module temperature 3",
	})
	moduleTemperature04Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: cacheNamespace,
		Name:      "module_Temperature_4",
		Help:      "module temperature 4",
	})

	acBridgeCurrentActiveMean01Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "ac_bridge_current_active_mean01",
		Help:      "AcBridgeCurrentActiveMean01",
	})
	acBridgeCurrentActiveMean02Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "ac_bridge_current_active_mean02",
		Help:      "AcBridgeCurrentActiveMean02",
	})
	acBridgeCurrentActiveMean03Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "ac_bridge_current_active_mean03",
		Help:      "AcBridgeCurrentActiveMean03",
	})
	acBridgeCurrentAcSumNowGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "ac_bridge_current_ac_sum_now",
		Help:      "AcBridgeCurrentAcSumNow",
	})
	acBridgeVoltageMean12Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "ac_bridge_voltage_mean12",
		Help:      "AcBridgeVoltageMean12",
	})
	acBridgeVoltageMean23Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "ac_bridge_voltage_mean23",
		Help:      "AcBridgeVoltageMean23",
	})
	acBridgeVoltageMean31Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "ac_bridge_voltage_mean31",
		Help:      "AcBridgeVoltageMean31",
	})
	componentsModeEnableGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "components_mode_enable",
		Help:      "ComponentsModeEnable",
	})
	componentsModeVisibleGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "components_mode_visible",
		Help:      "ComponentsModeVisible",
	})
	componentsTimeStampGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "components_time_stamp",
		Help:      "ComponentsTimeStamp",
	})
	gridFrequencyMeanGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "grid_frequency_mean",
		Help:      "GridFrequencyMean",
	})
	smartMeterEnergyActiveAbsolutMinusGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_energy_active_absolut_minus",
		Help:      "ins Netz eingespeister Strom",
	})
	smartMeterEnergyActiveAbsolutPlusGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_energy_active_absolut_plus",
		Help:      "aus dem Netz bezogener Strom",
	})
	smartMeterEnergyActiveConsumedSumGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_energy_active_consumed_sum",
		Help:      "aus dem Netz bezogener Strom",
	})
	smartMeterEnergyActiveProducedSumGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_energy_active_produced_sum",
		Help:      "ins Netz eingespeister Strom",
	})
	smartMeterEnergyReactiveConsumedSumGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_energy_reactive_consumed_sum",
		Help:      "SmartMeterEnergyReactiveConsumedSum",
	})
	smartMeterEnergyReactiveProducedSumGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_energy_reactive_produced_sum",
		Help:      "SmartMeterEnergyReactiveProducedSum",
	})
	smartMeterFactorPower01Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_factor_power01",
		Help:      "SmartMeterFactorPower01",
	})
	smartMeterFactorPower02Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_factor_power02",
		Help:      "SmartMeterFactorPower02",
	})
	smartMeterFactorPower03Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_factor_power03",
		Help:      "SmartMeterFactorPower03",
	})
	smartMeterFactorPowerSumGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_factor_power_sum",
		Help:      "SmartMeterFactorPowerSum",
	})
	smartMeterPowerActive01Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_power_active01",
		Help:      "SmartMeterPowerActive01",
	})
	smartMeterPowerActive02Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_power_active02",
		Help:      "SmartMeterPowerActive02",
	})
	smartMeterPowerActive03Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_power_active03",
		Help:      "SmartMeterPowerActive03",
	})
	smartMeterPowerActiveMean01Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_power_active_mean01",
		Help:      "SmartMeterPowerActiveMean01",
	})
	smartMeterPowerActiveMean02Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_power_active_mean02",
		Help:      "SmartMeterPowerActiveMean02",
	})
	smartMeterPowerActiveMean03Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_power_active_mean03",
		Help:      "SmartMeterPowerActiveMean03",
	})
	smartMeterPowerActiveMeanSumGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_power_active_mean_sum",
		Help:      "SmartMeterPowerActiveMeanSum",
	})
	smartMeterPowerApparent01Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_power_apparent01",
		Help:      "SmartMeterPowerApparent01",
	})
	smartMeterPowerApparent02Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_power_apparent02",
		Help:      "SmartMeterPowerApparent02",
	})
	smartMeterPowerApparent03Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_power_apparent03",
		Help:      "SmartMeterPowerApparent03",
	})
	smartMeterPowerApparentMean01Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_power_apparent_mean01",
		Help:      "SmartMeterPowerApparentMean01",
	})
	smartMeterPowerApparentMean02Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_power_apparent_mean02",
		Help:      "SmartMeterPowerApparentMean02",
	})
	smartMeterPowerApparentMean03Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_power_apparent_mean03",
		Help:      "SmartMeterPowerApparentMean03",
	})
	smartMeterPowerApparentMeanSumGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_power_apparent_mean_sum",
		Help:      "SmartMeterPowerApparentMeanSum",
	})
	smartMeterPowerReactive01Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_power_reactive01",
		Help:      "SmartMeterPowerReactive01",
	})
	smartMeterPowerReactive02Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_power_reactive02",
		Help:      "SmartMeterPowerReactive02",
	})
	smartMeterPowerReactive03Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_power_reactive03",
		Help:      "SmartMeterPowerReactive03",
	})
	smartMeterPowerReactiveMeanSumGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_power_reactive_mean_sum",
		Help:      "SmartMeterPowerReactiveMeanSum",
	})
	smartMeterVoltage01Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_voltage01",
		Help:      "SmartMeterVoltage01",
	})
	smartMeterVoltage02Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_voltage02",
		Help:      "SmartMeterVoltage02",
	})
	smartMeterVoltage03Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_voltage03",
		Help:      "SmartMeterVoltage03",
	})
	smartMeterVoltageMean01Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_voltage_mean01",
		Help:      "SmartMeterVoltageMean01",
	})
	smartMeterVoltageMean02Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_voltage_mean02",
		Help:      "SmartMeterVoltageMean02",
	})
	smartMeterVoltageMean03Gauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: meterNamespace,
		Name:      "smart_meter_voltage_mean03",
		Help:      "SmartMeterVoltageMean03",
	})
	gen24PvBatteryConsumptionSumGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: gen24Namespace,
		Name:      "energy_consumed_from_pv_and_battery_Ws_sum",
		Help:      "Total energy consumed from PV and Battery: battery discharged - battery charged + PV",
	})
	gen24GridConsumptionSumGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: gen24Namespace,
		Name:      "energy_consumed_from_grid_Wh_sum",
		Help:      "Total energy consumed from grid: injected - purchased",
	})
	gen24TotalConsumptionSumGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: gen24Namespace,
		Name:      "energy_consumption_total_Wh_sum",
		Help:      "Total energy consumed : from battery, from grid and from PV",
	})
	gen24timestampNowGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: gen24Namespace,
		Name:      "timestamp_now",
		Help:      "current timestamp",
	})
	batValueStateOfChargeRelativeGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: batteryNamespace,
		Name:      "state_of_charge_relative",
		Help:      "battery state of charge relative",
	})
	batValueStateOfHealthRelativeGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: batteryNamespace,
		Name:      "state_of_health_relative",
		Help:      "battery state of health relative",
	})
	batCapacityEstimationMaxGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: batteryNamespace,
		Name:      "capacity_estimation_max",
		Help:      "battery state of health relative",
	})
	batCapacityEstimationRemaining = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: batteryNamespace,
		Name:      "capacity_estimation_remaining",
		Help:      "capacity_estimation_remaining",
	})
	batEnergyactiveLifetimeCharged = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: batteryNamespace,
		Name:      "energy_active_lifetime_charged",
		Help:      "energy_active_lifetime_charged",
	})
	batEnergyactiveLifetimeDischarged = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: batteryNamespace,
		Name:      "energy_active_lifetime_discharged",
		Help:      "energy active lifetime discharged",
	})
	batTemperatureCell = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: batteryNamespace,
		Name:      "temperature_cell",
		Help:      "temperature cell",
	})
	batTemperatureCellMax = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: batteryNamespace,
		Name:      "temperature_cell_max",
		Help:      "temperature cell max",
	})
	batTemperatureCellMin = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: batteryNamespace,
		Name:      "temperature_cell_min",
		Help:      "temperature cell min",
	})
	componentsTimeStamp = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: batteryNamespace,
		Name:      "components_timestamp",
		Help:      "components timestamp",
	})
	dclinkPoweractiveLimitDischarge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: batteryNamespace,
		Name:      "dclink_poweractive_limit_discharge",
		Help:      "dclink poweractive limit discharge",
	})
	deviceTemperatureAmbienteMean = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: batteryNamespace,
		Name:      "device_temperature_ambiente_mean",
		Help:      "device temperature ambiente mean",
	})
)

func collectMetricsFromTarget(meterClient *fronius.MeterClient,
	symoClient *fronius.SymoClient,
	cacheClient *fronius.CacheClient,
	batteryClient *fronius.BatteryClient) {
	start := time.Now()
	log.WithFields(log.Fields{
		"url":     symoClient.Options.URL,
		"timeout": symoClient.Options.Timeout,
	}).Debug("Requesting data.")

	symoMetric(symoClient)
	batterMetric(batteryClient)

	meterData, meterErr := smartMeterMetric(meterClient)
	cacheData, cacheErr := cachingMetric(cacheClient)
	if meterErr == nil && cacheErr == nil {
		createCustomCalculatedMetrics(meterData, cacheData)
	}

	elapsed := time.Since(start)
	scrapeDurationGauge.Set(elapsed.Seconds())
}

func batterMetric(batteryClient *fronius.BatteryClient) {
	batteryData, batteryErr := batteryClient.GetBatteryData()
	if batteryErr != nil {
		log.WithError(batteryErr).Warn("Could not collect battery metrics.")
		scrapeErrorCount.Add(1)
	} else {
		parseBatteryMetrics(batteryData)
	}
}

func cachingMetric(cacheClient *fronius.CacheClient) (*fronius.CacheData, error) {
	cacheData, cacheErr := cacheClient.GetCacheData()
	if cacheErr != nil {
		log.WithError(cacheErr).Warn("Could not collect cache metrics.")
		scrapeErrorCount.Add(1)
	} else {
		parseCacheMetrics(cacheData)
	}
	return cacheData, cacheErr
}

func smartMeterMetric(meterClient *fronius.MeterClient) (*fronius.MeterData, error) {
	meterData, meterErr := meterClient.GetMeterData()
	if meterErr != nil {
		log.WithError(meterErr).Warn("Could not collect smartmeter metrics.")
		scrapeErrorCount.Add(1)
	} else {
		parseMeterMetrics(meterData)
	}
	return meterData, meterErr
}

func symoMetric(symoClient *fronius.SymoClient) {
	data, symoErr := symoClient.GetPowerFlowData()
	if symoErr != nil {
		log.WithError(symoErr).Warn("Could not collect symo metrics.")
		scrapeErrorCount.Add(1)
	} else {
		parseMetrics(data)
	}
}

func parseBatteryMetrics(data *fronius.BatteryData) {
	batValueStateOfChargeRelativeGauge.Set(data.Channels.BatValueStateOfChargeRelative)
	batValueStateOfHealthRelativeGauge.Set(data.Channels.BatValueStateOfHealthRelative)
	batCapacityEstimationMaxGauge.Set(data.Channels.BatCapacityEstimationMaxF64)
	batCapacityEstimationRemaining.Set(data.Channels.BatCapacityEstimationRemainingF64)
	batEnergyactiveLifetimeCharged.Set(data.Channels.BatEnergyactiveLifetimeChargedF64)
	batEnergyactiveLifetimeDischarged.Set(data.Channels.BatEnergyactiveLifetimeDischargedF64)
	batTemperatureCell.Set(data.Channels.BatTemperatureCellF64)
	batTemperatureCellMax.Set(data.Channels.BatTemperatureCellMaxF64)
	batTemperatureCellMin.Set(data.Channels.BatTemperatureCellMinF64)
	componentsTimeStamp.Set(data.Channels.ComponentsTimeStampU64)
	dclinkPoweractiveLimitDischarge.Set(data.Channels.DclinkPoweractiveLimitDischargeF64)
	deviceTemperatureAmbienteMean.Set(data.Channels.DeviceTemperatureAmbientemeanF32)
}

func createCustomCalculatedMetrics(meterData *fronius.MeterData, cacheData *fronius.CacheData) {
	//Help:      "Total energy consumed calculated as: battery discharged -
	//													battery charged +
	//													PV(1+2) -
	//													injected +
	//													purchased",
	pvBatteryBalance := cacheData.Channels.BatEnergyactiveActivedischargeSum01 -
		cacheData.Channels.BatEnergyactiveActivechargeSum01 +
		cacheData.Channels.PvEnergyactiveActiveSum01 +
		cacheData.Channels.PvEnergyactiveActiveSum02
	gen24PvBatteryConsumptionSumGauge.Set(pvBatteryBalance)
	gridConsumption := meterData.SmartMeterEnergyActiveConsumedSum -
		meterData.SmartMeterEnergyActiveProducedSum
	gen24GridConsumptionSumGauge.Set(gridConsumption)
	gen24TotalConsumptionSumGauge.Set(pvBatteryBalance + gridConsumption)

	sec := time.Now().Unix() // number of seconds since January 1, 1970 UTC
	gen24timestampNowGauge.Set(float64(sec))
}

func parseCacheMetrics(data *fronius.CacheData) {
	log.WithField("data", *data).Debug("Parsing data.")
	//acbridgeEnergyactiveActiveconsumedSum01Gauge.Set(data.Channels.AcbridgeEnergyactiveActiveconsumedSum01)
	//acbridgeEnergyactiveActiveconsumedSum02Gauge.Set(data.Channels.AcbridgeEnergyactiveActiveconsumedSum02)
	//acbridgeEnergyactiveActiveconsumedSum03Gauge.Set(data.Channels.AcbridgeEnergyactiveActiveconsumedSum03)
	//acbridgeEnergyactiveProducedSum01Gauge.Set(data.Channels.AcbridgeEnergyactiveProducedSum01)
	//acbridgeEnergyactiveProducedSum02Gauge.Set(data.Channels.AcbridgeEnergyactiveProducedSum02)
	//acbridgeEnergyactiveProducedSum03Gauge.Set(data.Channels.AcbridgeEnergyactiveProducedSum03)
	// Verbrauch aus Eigenproduktion
	acbridgePoweractiveSumMeanF32Gauge.Set(data.Channels.AcbridgePoweractiveSumMeanF32)
	// Verbrauch aus Eigenproduktion
	acbridgePowerapparentSumMeanF32Gauge.Set(data.Channels.AcbridgePowerapparentSumMeanF32)
	// Verbrauch aus Eigenproduktion
	acbridgePowerreactiveSumMeanF32Gauge.Set(data.Channels.AcbridgePowerreactiveSumMeanF32)
	//acbridgeTimeBackupmodeUptimeSumF32Gauge.Set(data.Channels.AcbridgeTimeBackupmodeUptimeSumF32)
	// Batterie laden
	batEnergyactiveActivechargeSum01Gauge.Set(data.Channels.BatEnergyactiveActivechargeSum01)
	// Batterie entladen
	batEnergyactiveActivedischargeSum01Gauge.Set(data.Channels.BatEnergyactiveActivedischargeSum01)
	//deviceTimeUptimeSumF32Gauge.Set(data.Channels.DeviceTimeUptimeSumF32)
	// Produktion der PV-Anlage (vermutlich String 1)
	pvEnergyactiveActiveSum01Gauge.Set(data.Channels.PvEnergyactiveActiveSum01)
	// Produktion der PV-Anlage (vermutlich String 2)
	pvEnergyactiveActiveSum02Gauge.Set(data.Channels.PvEnergyactiveActiveSum02)
	// Produktion der PV-Anlage Gesamt (1+2)
	pvEnergyactiveActiveSumGauge.Set(data.Channels.PvEnergyactiveActiveSum01 + data.Channels.PvEnergyactiveActiveSum02)
	// Temperatures
	deviceTemperatureAmbientGauge.Set(data.Channels.DeviceTemperatureAmbiente)
	moduleTemperature01Gauge.Set(data.Channels.ModuleTemperature01)
	moduleTemperature03Gauge.Set(data.Channels.ModuleTemperature03)
	moduleTemperature04Gauge.Set(data.Channels.ModuleTemperature04)
}

func parseMeterMetrics(data *fronius.MeterData) {
	log.WithField("data", *data).Debug("Parsing data.")
	//acBridgeCurrentActiveMean01Gauge.Set(data.AcBridgeCurrentActiveMean01)
	//acBridgeCurrentActiveMean02Gauge.Set(data.AcBridgeCurrentActiveMean02)
	//acBridgeCurrentActiveMean03Gauge.Set(data.AcBridgeCurrentActiveMean03)
	acBridgeCurrentAcSumNowGauge.Set(data.AcBridgeCurrentAcSumNow)
	//acBridgeVoltageMean12Gauge.Set(data.AcBridgeVoltageMean12)
	//acBridgeVoltageMean23Gauge.Set(data.AcBridgeVoltageMean23)
	//acBridgeVoltageMean31Gauge.Set(data.AcBridgeVoltageMean31)
	//componentsModeEnableGauge.Set(data.ComponentsModeEnable)
	//componentsModeVisibleGauge.Set(data.ComponentsModeVisible)
	//componentsTimeStampGauge.Set(data.ComponentsTimeStamp)
	gridFrequencyMeanGauge.Set(data.GridFrequencyMean)
	//smartMeterEnergyActiveAbsolutMinusGauge.Set(data.SmartMeterEnergyActiveAbsolutMinus)
	//smartMeterEnergyActiveAbsolutPlusGauge.Set(data.SmartMeterEnergyActiveAbsolutPlus)
	smartMeterEnergyActiveConsumedSumGauge.Set(data.SmartMeterEnergyActiveConsumedSum)
	smartMeterEnergyActiveProducedSumGauge.Set(data.SmartMeterEnergyActiveProducedSum)
	//smartMeterEnergyReactiveConsumedSumGauge.Set(data.SmartMeterEnergyReactiveConsumedSum)
	//smartMeterEnergyReactiveProducedSumGauge.Set(data.SmartMeterEnergyReactiveProducedSum)
	//smartMeterFactorPower01Gauge.Set(data.SmartMeterFactorPower01)
	//smartMeterFactorPower02Gauge.Set(data.SmartMeterFactorPower02)
	//smartMeterFactorPower03Gauge.Set(data.SmartMeterFactorPower03)
	//smartMeterFactorPowerSumGauge.Set(data.SmartMeterFactorPowerSum)
	//smartMeterPowerActive01Gauge.Set(data.SmartMeterPowerActive01)
	//smartMeterPowerActive02Gauge.Set(data.SmartMeterPowerActive02)
	//smartMeterPowerActive03Gauge.Set(data.SmartMeterPowerActive03)
	//smartMeterPowerActiveMean01Gauge.Set(data.SmartMeterPowerActiveMean01)
	//smartMeterPowerActiveMean02Gauge.Set(data.SmartMeterPowerActiveMean02)
	//smartMeterPowerActiveMean03Gauge.Set(data.SmartMeterPowerActiveMean03)
	//smartMeterPowerActiveMeanSumGauge.Set(data.SmartMeterPowerActiveMeanSum)
	//smartMeterPowerApparent01Gauge.Set(data.SmartMeterPowerApparent01)
	//smartMeterPowerApparent02Gauge.Set(data.SmartMeterPowerApparent02)
	//smartMeterPowerApparent03Gauge.Set(data.SmartMeterPowerApparent03)
	//smartMeterPowerApparentMean01Gauge.Set(data.SmartMeterPowerApparentMean01)
	//smartMeterPowerApparentMean02Gauge.Set(data.SmartMeterPowerApparentMean02)
	//smartMeterPowerApparentMean03Gauge.Set(data.SmartMeterPowerApparentMean03)
	smartMeterPowerApparentMeanSumGauge.Set(data.SmartMeterPowerApparentMeanSum)
	//smartMeterPowerReactive01Gauge.Set(data.SmartMeterPowerReactive01)
	//smartMeterPowerReactive02Gauge.Set(data.SmartMeterPowerReactive02)
	//smartMeterPowerReactive03Gauge.Set(data.SmartMeterPowerReactive03)
	//smartMeterPowerReactiveMeanSumGauge.Set(data.SmartMeterPowerReactiveMeanSum)
	//smartMeterVoltage01Gauge.Set(data.SmartMeterVoltage01)
	//smartMeterVoltage02Gauge.Set(data.SmartMeterVoltage02)
	//smartMeterVoltage03Gauge.Set(data.SmartMeterVoltage03)
	//smartMeterVoltageMean01Gauge.Set(data.SmartMeterVoltageMean01)
	//smartMeterVoltageMean02Gauge.Set(data.SmartMeterVoltageMean02)
	//smartMeterVoltageMean03Gauge.Set(data.SmartMeterVoltageMean03)
}

func parseMetrics(data *fronius.SymoData) {
	log.WithField("data", *data).Debug("Parsing data.")
	for key, inverter := range data.Inverters {
		inverterPowerGaugeVec.WithLabelValues(key).Set(inverter.Power)
		inverterSOCGauge.WithLabelValues(key).Set(inverter.SOC)
	}
	sitePowerAccuGauge.Set(data.Site.PowerAccu)
	sitePowerGridGauge.Set(data.Site.PowerGrid)
	sitePowerGridInjectGauge.Set(heavySide(-data.Site.PowerGrid))
	sitePowerGridPurchaseGauge.Set(heavySide(data.Site.PowerGrid))
	sitePowerLoadGauge.Set(data.Site.PowerLoad)
	sitePowerPhotovoltaicsGauge.Set(data.Site.PowerPhotovoltaic)

	//siteEnergyGaugeVec.WithLabelValues("day").Set(data.Site.EnergyDay)
	//siteEnergyGaugeVec.WithLabelValues("year").Set(data.Site.EnergyYear)
	//siteEnergyGaugeVec.WithLabelValues("total").Set(data.Site.EnergyTotal)

	siteAutonomyRatioGauge.Set(data.Site.RelativeAutonomy)
	siteSelfConsumptionRatioGauge.Set(data.Site.RelativeSelfConsumption)
}

func heavySide(value float64) float64 {
	returnValue := 0.
	if value >= 0 {
		returnValue = value
	}
	return returnValue
}
