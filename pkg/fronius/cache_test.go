package fronius

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func Test_CacheData_GivenUrl_WhenRequestData_ThenParseStruct(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		payload, err := ioutil.ReadFile("testdata/cache-example2.json")
		require.NoError(t, err)
		rw.Write(payload)
	}))

	c, err := NewCacheClient(CacheClientOptions{
		URL: server.URL,
	})
	require.NoError(t, err)

	p, err := c.GetCacheData()
	assert.NoError(t, err)
	assert.Equal(t, 14630.0, p.Channels.AcbridgeEnergyactiveActiveconsumedSum01)
	assert.Equal(t, 14933.0, p.Channels.AcbridgeEnergyactiveActiveconsumedSum02)
	assert.Equal(t, 13445.0, p.Channels.AcbridgeEnergyactiveActiveconsumedSum03)
	assert.Equal(t, 2871847800.0, p.Channels.AcbridgeEnergyactiveProducedSum01)
	assert.Equal(t, 2859973671.0, p.Channels.AcbridgeEnergyactiveProducedSum02)
	assert.Equal(t, 2852974727.0, p.Channels.AcbridgeEnergyactiveProducedSum03)
	assert.Equal(t, 791.610107421875, p.Channels.AcbridgePoweractiveSumMeanF32)
	assert.Equal(t, 791.67822265625, p.Channels.AcbridgePowerapparentSumMeanF32)
	assert.Equal(t, -1.9869692325592041, p.Channels.AcbridgePowerreactiveSumMeanF32)
	assert.Equal(t, 0.0, p.Channels.AcbridgeTimeBackupmodeUptimeSumF32)
	assert.Equal(t, 1030922824.0, p.Channels.BatEnergyactiveActivechargeSum01)
	assert.Equal(t, 941355315.0, p.Channels.BatEnergyactiveActivedischargeSum01)
	assert.Equal(t, 4520318.0, p.Channels.DeviceTimeUptimeSumF32)
	assert.Equal(t, 4459474804.0, p.Channels.PvEnergyactiveActiveSum01)
	assert.Equal(t, 4510510888.0, p.Channels.PvEnergyactiveActiveSum02)
	assert.Equal(t, 50.88671875, p.Channels.DeviceTemperatureAmbiente)
	assert.Equal(t, 39.55035400390625, p.Channels.ModuleTemperature01)
	assert.Equal(t, 36.107666015625, p.Channels.ModuleTemperature03)
	assert.Equal(t, 35.998870849609375, p.Channels.ModuleTemperature04)
}
