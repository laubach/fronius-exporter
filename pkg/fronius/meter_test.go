package fronius

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func Test_MeterData_GivenUrl_WhenRequestData_ThenParseStruct(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		payload, err := ioutil.ReadFile("testdata/meter-example.json")
		require.NoError(t, err)
		rw.Write(payload)
	}))

	c, err := NewMeterClient(MeterClientOptions{
		URL: server.URL,
	})
	require.NoError(t, err)

	p, err := c.GetMeterData()
	assert.NoError(t, err)
	assert.Equal(t, 0.778, p.AcBridgeCurrentActiveMean01)
	assert.Equal(t, 1.006, p.AcBridgeCurrentActiveMean02)
	assert.Equal(t, -0.61199999999999999, p.AcBridgeCurrentActiveMean03)
	assert.Equal(t, 1.1720000000, p.AcBridgeCurrentAcSumNow)
	assert.Equal(t, 404.30000000, p.AcBridgeVoltageMean12)
	assert.Equal(t, 406.3000000, p.AcBridgeVoltageMean23)
	assert.Equal(t, 402.0, p.AcBridgeVoltageMean31)
	assert.Equal(t, 1.0, p.ComponentsModeEnable)
	assert.Equal(t, 1.0, p.ComponentsModeVisible)
	assert.Equal(t, 1625947386.0, p.ComponentsTimeStamp)
	assert.Equal(t, 1652346.0, p.SmartMeterEnergyActiveAbsolutMinus)
	assert.Equal(t, 271802.0, p.SmartMeterEnergyActiveAbsolutPlus)
	assert.Equal(t, 271802.0, p.SmartMeterEnergyActiveConsumedSum)
	assert.Equal(t, 1652346.0, p.SmartMeterEnergyActiveProducedSum)
	assert.Equal(t, 131123.0, p.SmartMeterEnergyReactiveConsumedSum)
	assert.Equal(t, 236981.0, p.SmartMeterEnergyReactiveProducedSum)
	assert.Equal(t, "Fronius", p.Details.Manufacturer)
	assert.Equal(t, "Smart Meter TS 65A-3", p.Details.Model)
	assert.Equal(t, "2073166302", p.Details.Serial)
}
