package fronius

import (
	"encoding/json"
	"net/http"
	"net/url"
	"time"
)

type (
	cacheFlow struct {
		Body struct {
			Data CacheData
		}
	}
	// CacheData holds the parsed data from the Symo API.
	CacheData struct {
		Channels Channels `json:"channels"`
	}
	Channels struct {
		AcbridgeEnergyactiveActiveconsumedSum01 float64 `json:"ACBRIDGE_ENERGYACTIVE_ACTIVECONSUMED_SUM_01_U64"`
		AcbridgeEnergyactiveActiveconsumedSum02 float64 `json:"ACBRIDGE_ENERGYACTIVE_ACTIVECONSUMED_SUM_02_U64"`
		AcbridgeEnergyactiveActiveconsumedSum03 float64 `json:"ACBRIDGE_ENERGYACTIVE_ACTIVECONSUMED_SUM_03_U64"`
		AcbridgeEnergyactiveProducedSum01       float64 `json:"ACBRIDGE_ENERGYACTIVE_PRODUCED_SUM_01_U64"`
		AcbridgeEnergyactiveProducedSum02       float64 `json:"ACBRIDGE_ENERGYACTIVE_PRODUCED_SUM_02_U64"`
		AcbridgeEnergyactiveProducedSum03       float64 `json:"ACBRIDGE_ENERGYACTIVE_PRODUCED_SUM_03_U64"`
		AcbridgePoweractiveSumMeanF32           float64 `json:"ACBRIDGE_POWERACTIVE_SUM_MEAN_F32"`
		AcbridgePowerapparentSumMeanF32         float64 `json:"ACBRIDGE_POWERAPPARENT_SUM_MEAN_F32"`
		AcbridgePowerreactiveSumMeanF32         float64 `json:"ACBRIDGE_POWERREACTIVE_SUM_MEAN_F32"`
		AcbridgeTimeBackupmodeUptimeSumF32      float64 `json:"ACBRIDGE_TIME_BACKUPMODE_UPTIME_SUM_F32"`
		BatEnergyactiveActivechargeSum01        float64 `json:"BAT_ENERGYACTIVE_ACTIVECHARGE_SUM_01_U64"`
		BatEnergyactiveActivedischargeSum01     float64 `json:"BAT_ENERGYACTIVE_ACTIVEDISCHARGE_SUM_01_U64"`
		DeviceTimeUptimeSumF32                  float64 `json:"DEVICE_TIME_UPTIME_SUM_F32"`
		PvEnergyactiveActiveSum01               float64 `json:"PV_ENERGYACTIVE_ACTIVE_SUM_01_U64"`
		PvEnergyactiveActiveSum02               float64 `json:"PV_ENERGYACTIVE_ACTIVE_SUM_02_U64"`
		DeviceTemperatureAmbiente               float64 `json:"DEVICE_TEMPERATURE_AMBIENTEMEAN_F32"`
		ModuleTemperature01                     float64 `json:"MODULE_TEMPERATURE_MEAN_01_F32"`
		ModuleTemperature03                     float64 `json:"MODULE_TEMPERATURE_MEAN_03_F32"`
		ModuleTemperature04                     float64 `json:"MODULE_TEMPERATURE_MEAN_04_F32"`
	}
	// CacheClient is a wrapper for making API requests against a Fronius Fronius Gen24 Wechselrichter CACHE.
	CacheClient struct {
		request *http.Request
		Options CacheClientOptions
	}
	// CacheClientOptions holds some parameters for the CacheClient.
	CacheClientOptions struct {
		URL     string
		Headers http.Header
		Timeout time.Duration
	}
)

// NewCacheClient constructs a CacheClient ready to use for collecting metrics.
func NewCacheClient(options CacheClientOptions) (*CacheClient, error) {
	u, err := url.Parse(options.URL)
	if err != nil {
		return nil, err
	}
	return &CacheClient{
		request: &http.Request{
			URL:    u,
			Header: options.Headers,
		},
		Options: options,
	}, nil
}

// GetCacheData returns the parsed data from the Gen24 Meter device.
func (c *CacheClient) GetCacheData() (*CacheData, error) {
	client := http.DefaultClient
	client.Timeout = c.Options.Timeout
	response, err := client.Do(c.request)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	p := cacheFlow{}
	err = json.NewDecoder(response.Body).Decode(&p)
	if err != nil {
		return nil, err
	}
	return &p.Body.Data, nil
}
