package fronius

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func Test_BatteryData_GivenUrl_WhenRequestData_ThenParseStruct(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		payload, err := ioutil.ReadFile("testdata/battery-example.json")
		require.NoError(t, err)
		rw.Write(payload)
	}))

	c, err := NewBatteryClient(BatteryClientOptions{
		URL: server.URL,
	})
	require.NoError(t, err)

	p, err := c.GetBatteryData()
	assert.NoError(t, err)
	assert.Equal(t, 10400.0, p.Channels.BatCapacityEstimationMaxF64)
	assert.Equal(t, 5782.0, p.Channels.BatCapacityEstimationRemainingF64)
	assert.Equal(t, -0.79866888519134782, p.Channels.BatCurrentDcF64)
	assert.Equal(t, -0.79904875148632581, p.Channels.BatCurrentDcInternalF64)
	assert.Equal(t, 345416.0, p.Channels.BatEnergyactiveLifetimeChargedF64)
	assert.Equal(t, 275827.0, p.Channels.BatEnergyactiveLifetimeDischargedF64)
	assert.Equal(t, 3.0, p.Channels.BatModeCellStateU16)
	assert.Equal(t, 1.0, p.Channels.BatModeHybridOperatingStateU16)
	assert.Equal(t, 0.0, p.Channels.BatModeStateU16)
	assert.Equal(t, 2.0, p.Channels.BatModeU16)
	assert.Equal(t, 1.0, p.Channels.BatModeWakeEnableStatusU16)
	assert.Equal(t, 22.0, p.Channels.BatTemperatureCellF64)
	assert.Equal(t, 23.0, p.Channels.BatTemperatureCellMaxF64)
	assert.Equal(t, 21.0, p.Channels.BatTemperatureCellMinF64)
	assert.Equal(t, 55.600000000000001, p.Channels.BatValueStateOfChargeRelative)
	assert.Equal(t, 100.0, p.Channels.BatValueStateOfHealthRelative)
	assert.Equal(t, 420.5, p.Channels.BatVoltageDcInternalF64)
	assert.Equal(t, 1.0, p.Channels.ComponentsModeEnableU16)
	assert.Equal(t, 1.0, p.Channels.ComponentsModeVisibleU16)
	assert.Equal(t, 1627163642.0, p.Channels.ComponentsTimeStampU64)
	assert.Equal(t, 10764.0, p.Channels.DclinkPoweractiveLimitDischargeF64)
	assert.Equal(t, 10841.0, p.Channels.DclinkPoweractiveMaxF32)
	assert.Equal(t, 420.69999999999999, p.Channels.DclinkVoltageMeanF32)
	assert.Equal(t, 25.0, p.Channels.DeviceTemperatureAmbientemeanF32)
}
