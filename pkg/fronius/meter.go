package fronius

import (
	"encoding/json"
	"net/http"
	"net/url"
	"time"
)

type (
	meterFlow struct {
		Body struct {
			Data MeterData
		}
	}
	// MeterData holds the parsed data from the Symo API.
	MeterData struct {
		Details                             Details `json:"Details"`
		AcBridgeCurrentActiveMean01         float64 `json:"ACBRIDGE_CURRENT_ACTIVE_MEAN_01_F32"`
		AcBridgeCurrentActiveMean02         float64 `json:"ACBRIDGE_CURRENT_ACTIVE_MEAN_02_F32"`
		AcBridgeCurrentActiveMean03         float64 `json:"ACBRIDGE_CURRENT_ACTIVE_MEAN_03_F32"`
		AcBridgeCurrentAcSumNow             float64 `json:"ACBRIDGE_CURRENT_AC_SUM_NOW_F64"`
		AcBridgeVoltageMean12               float64 `json:"ACBRIDGE_VOLTAGE_MEAN_12_F32"`
		AcBridgeVoltageMean23               float64 `json:"ACBRIDGE_VOLTAGE_MEAN_23_F32"`
		AcBridgeVoltageMean31               float64 `json:"ACBRIDGE_VOLTAGE_MEAN_31_F32"`
		ComponentsModeEnable                float64 `json:"COMPONENTS_MODE_ENABLE_U16"`
		ComponentsModeVisible               float64 `json:"COMPONENTS_MODE_VISIBLE_U16"`
		ComponentsTimeStamp                 float64 `json:"COMPONENTS_TIME_STAMP_U64"`
		GridFrequencyMean                   float64 `json:"GRID_FREQUENCY_MEAN_F32"`
		SmartMeterEnergyActiveAbsolutMinus  float64 `json:"SMARTMETER_ENERGYACTIVE_ABSOLUT_MINUS_F64"`
		SmartMeterEnergyActiveAbsolutPlus   float64 `json:"SMARTMETER_ENERGYACTIVE_ABSOLUT_PLUS_F64"`
		SmartMeterEnergyActiveConsumedSum   float64 `json:"SMARTMETER_ENERGYACTIVE_CONSUMED_SUM_F64"`
		SmartMeterEnergyActiveProducedSum   float64 `json:"SMARTMETER_ENERGYACTIVE_PRODUCED_SUM_F64"`
		SmartMeterEnergyReactiveConsumedSum float64 `json:"SMARTMETER_ENERGYREACTIVE_CONSUMED_SUM_F64"`
		SmartMeterEnergyReactiveProducedSum float64 `json:"SMARTMETER_ENERGYREACTIVE_PRODUCED_SUM_F64"`
		SmartMeterFactorPower01             float64 `json:"SMARTMETER_FACTOR_POWER_01_F64"`
		SmartMeterFactorPower02             float64 `json:"SMARTMETER_FACTOR_POWER_02_F64"`
		SmartMeterFactorPower03             float64 `json:"SMARTMETER_FACTOR_POWER_03_F64"`
		SmartMeterFactorPowerSum            float64 `json:"SMARTMETER_FACTOR_POWER_SUM_F64"`
		SmartMeterPowerActive01             float64 `json:"SMARTMETER_POWERACTIVE_01_F64"`
		SmartMeterPowerActive02             float64 `json:"SMARTMETER_POWERACTIVE_02_F64"`
		SmartMeterPowerActive03             float64 `json:"SMARTMETER_POWERACTIVE_03_F64"`
		SmartMeterPowerActiveMean01         float64 `json:"SMARTMETER_POWERACTIVE_MEAN_01_F64"`
		SmartMeterPowerActiveMean02         float64 `json:"SMARTMETER_POWERACTIVE_MEAN_02_F64"`
		SmartMeterPowerActiveMean03         float64 `json:"SMARTMETER_POWERACTIVE_MEAN_03_F64"`
		SmartMeterPowerActiveMeanSum        float64 `json:"SMARTMETER_POWERACTIVE_MEAN_SUM_F64"`
		SmartMeterPowerApparent01           float64 `json:"SMARTMETER_POWERAPPARENT_01_F64"`
		SmartMeterPowerApparent02           float64 `json:"SMARTMETER_POWERAPPARENT_02_F64"`
		SmartMeterPowerApparent03           float64 `json:"SMARTMETER_POWERAPPARENT_03_F64"`
		SmartMeterPowerApparentMean01       float64 `json:"SMARTMETER_POWERAPPARENT_MEAN_01_F64"`
		SmartMeterPowerApparentMean02       float64 `json:"SMARTMETER_POWERAPPARENT_MEAN_02_F64"`
		SmartMeterPowerApparentMean03       float64 `json:"SMARTMETER_POWERAPPARENT_MEAN_03_F64"`
		SmartMeterPowerApparentMeanSum      float64 `json:"SMARTMETER_POWERAPPARENT_MEAN_SUM_F64"`
		SmartMeterPowerReactive01           float64 `json:"SMARTMETER_POWERREACTIVE_01_F64"`
		SmartMeterPowerReactive02           float64 `json:"SMARTMETER_POWERREACTIVE_02_F64"`
		SmartMeterPowerReactive03           float64 `json:"SMARTMETER_POWERREACTIVE_03_F64"`
		SmartMeterPowerReactiveMeanSum      float64 `json:"SMARTMETER_POWERREACTIVE_MEAN_SUM_F64"`
		SmartMeterVoltage01                 float64 `json:"SMARTMETER_VOLTAGE_01_F64"`
		SmartMeterVoltage02                 float64 `json:"SMARTMETER_VOLTAGE_02_F64"`
		SmartMeterVoltage03                 float64 `json:"SMARTMETER_VOLTAGE_03_F64"`
		SmartMeterVoltageMean01             float64 `json:"SMARTMETER_VOLTAGE_MEAN_01_F64"`
		SmartMeterVoltageMean02             float64 `json:"SMARTMETER_VOLTAGE_MEAN_02_F64"`
		SmartMeterVoltageMean03             float64 `json:"SMARTMETER_VOLTAGE_MEAN_03_F64"`
	}
	// Details about the meter.
	Details struct {
		Manufacturer string `json:"Manufacturer"`
		Model        string `json:"Model"`
		Serial       string `json:"Serial"`
	}
	// MeterClient is a wrapper for making API requests against a Fronius Meter device (via Fronius Gen24 Wechselrichter).
	MeterClient struct {
		request *http.Request
		Options MeterClientOptions
	}
	// MeterClientOptions holds some parameters for the MeterClient.
	MeterClientOptions struct {
		URL     string
		Headers http.Header
		Timeout time.Duration
	}
)

// NewMeterClient constructs a MeterClient ready to use for collecting metrics.
func NewMeterClient(options MeterClientOptions) (*MeterClient, error) {
	u, err := url.Parse(options.URL)
	if err != nil {
		return nil, err
	}
	return &MeterClient{
		request: &http.Request{
			URL:    u,
			Header: options.Headers,
		},
		Options: options,
	}, nil
}

// GetMeterData returns the parsed data from the Gen24 Meter device.
func (c *MeterClient) GetMeterData() (*MeterData, error) {
	client := http.DefaultClient
	client.Timeout = c.Options.Timeout
	response, err := client.Do(c.request)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	p := meterFlow{}
	err = json.NewDecoder(response.Body).Decode(&p)
	if err != nil {
		return nil, err
	}
	return &p.Body.Data, nil
}
